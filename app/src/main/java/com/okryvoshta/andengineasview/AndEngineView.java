package com.okryvoshta.andengineasview;

import android.app.Activity;
import android.content.Context;
import android.hardware.SensorManager;
import android.os.PowerManager;
import android.util.AttributeSet;

import com.badlogic.gdx.math.Vector2;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.ConfigChooserOptions;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.view.IRendererListener;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.IGameInterface;
import org.andengine.util.ActivityUtils;
import org.andengine.util.Constants;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import java.io.IOException;

public class AndEngineView extends RenderSurfaceView implements IRendererListener {

    private int CAMERA_WIDTH = 0;
    private int CAMERA_HEIGHT = 0;

    private Activity mActivity;
    private Engine mEngine;

    private boolean mGamePaused;
    private boolean mGameCreated;
    private boolean mCreateGameCalled;
    private boolean mOnReloadResourcesScheduled;

    private TiledTextureRegion mHelicopterTextureRegion;

    private PowerManager.WakeLock mWakeLock;

    public AndEngineView(Context pContext) {
        super(pContext);
    }

    public AndEngineView(Context pContext, AttributeSet pAttrs) {
        super(pContext, pAttrs);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mGamePaused = true;

        mActivity = (Activity) getContext();

        mEngine = new Engine(onCreateEngineOptions());
        mEngine.startUpdateThread();

        setZOrderOnTop(true);
        setRenderer(mEngine, this);
    }

    @Override
    protected void onMeasure(int pWidthMeasureSpec, int pHeightMeasureSpec) {
        CAMERA_WIDTH = MeasureSpec.getSize(pWidthMeasureSpec) / 4;
        CAMERA_HEIGHT = MeasureSpec.getSize(pHeightMeasureSpec) / 4;
        mEngine.getCamera().set(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        super.onMeasure(pWidthMeasureSpec, pHeightMeasureSpec);
    }

    protected Scene onCreateScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());

        Scene mScene = new Scene();
        mScene.getBackground().setColor(Color.TRANSPARENT);

		/* Continuously flying helicopter. */
        final AnimatedSprite helicopter = new AnimatedSprite(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2, this.mHelicopterTextureRegion, mEngine.getVertexBufferObjectManager());
        helicopter.animate(new long[]{100, 100}, 1, 2, true);
        mScene.attachChild(helicopter);

        PhysicsWorld mPhysicsWorld = new PhysicsWorld(new Vector2(SensorManager.GRAVITY_EARTH, SensorManager.GRAVITY_EARTH), false);
        mScene.registerUpdateHandler(mPhysicsWorld);

        return mScene;
    }

    protected void onCreateResources() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        BuildableBitmapTextureAtlas mBitmapTextureAtlas = new BuildableBitmapTextureAtlas(mEngine.getTextureManager(), 512, 256, TextureOptions.NEAREST);

        this.mHelicopterTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mBitmapTextureAtlas, mActivity, "helicopter_tiled.png", 2, 2);

        try {
            mBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
            mBitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }
    }

    public EngineOptions onCreateEngineOptions() {
        final Camera camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.PORTRAIT_SENSOR, new FillResolutionPolicy(), camera);

        final ConfigChooserOptions configChooserOptions = engineOptions.getRenderOptions().getConfigChooserOptions();
        configChooserOptions.setRequestedRedSize(8);
        configChooserOptions.setRequestedGreenSize(8);
        configChooserOptions.setRequestedBlueSize(8);
        configChooserOptions.setRequestedAlphaSize(8);
        configChooserOptions.setRequestedDepthSize(16);

        return engineOptions;
    }

    @Override
    public void onSurfaceCreated(GLState pGlState) {
        if (mGameCreated) {
            onReloadResources();

            if (mGamePaused && mGameCreated && !isFinishing()) {
                onResumeGame();
            }
        } else {
            if (mCreateGameCalled) {
                mOnReloadResourcesScheduled = true;
            } else {
                mCreateGameCalled = true;
                onCreateGame();
            }
        }
    }

    private boolean isFinishing() {
        return mActivity.isFinishing();
    }

    @Override
    public void onSurfaceChanged(GLState pGlState, int pWidth, int pHeight) {

    }

    protected synchronized void onCreateGame() {
        final IGameInterface.OnPopulateSceneCallback onPopulateSceneCallback = new IGameInterface.OnPopulateSceneCallback() {
            @Override
            public void onPopulateSceneFinished() {
                try {
                    AndEngineView.this.onGameCreated();
                } catch (final Throwable ignored) {
                }

                AndEngineView.this.callGameResumedOnUIThread();
            }
        };

        final IGameInterface.OnCreateSceneCallback onCreateSceneCallback = new IGameInterface.OnCreateSceneCallback() {
            @Override
            public void onCreateSceneFinished(final Scene pScene) {
                AndEngineView.this.mEngine.setScene(pScene);

                try {
                    AndEngineView.this.onPopulateScene(pScene, onPopulateSceneCallback);
                } catch (final Throwable ignored) {
                }
            }
        };

        final IGameInterface.OnCreateResourcesCallback onCreateResourcesCallback = new IGameInterface.OnCreateResourcesCallback() {
            @Override
            public void onCreateResourcesFinished() {
                try {
                    AndEngineView.this.onCreateScene(onCreateSceneCallback);
                } catch (final Throwable ignored) {
                }
            }
        };

        try {
            if (org.andengine.BuildConfig.DEBUG) {
                Debug.d(this.getClass().getSimpleName() + ".onCreateResources" + " @(Thread: '" + Thread.currentThread().getName() + "')");
            }

            this.onCreateResources(onCreateResourcesCallback);
        } catch (final Throwable ignored) {
        }
    }

    public synchronized void onGameCreated() {
        mGameCreated = true;

		/* Since the potential asynchronous resource creation,
         * the surface might already be invalid
		 * and a resource reloading might be necessary. */
        if (this.mOnReloadResourcesScheduled) {
            this.mOnReloadResourcesScheduled = false;
            try {
                this.onReloadResources();
            } catch (final Throwable pThrowable) {
                Debug.e(this.getClass().getSimpleName() + ".onReloadResources failed." + " @(Thread: '" + Thread.currentThread().getName() + "')", pThrowable);
            }
        }
    }

    public void onReloadResources() {
        this.mEngine.onReloadResources();
    }

    public synchronized void onResumeGame() {
        mEngine.start();
        mGamePaused = false;
    }

    private void callGameResumedOnUIThread() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!AndEngineView.this.isFinishing()) {
                    AndEngineView.this.onResumeGame();
                }
            }
        });
    }

    //    @Override
    public void onCreateResources(IGameInterface.OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
        this.onCreateResources();

        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    //    @Override
    public void onCreateScene(IGameInterface.OnCreateSceneCallback pOnCreateSceneCallback) throws IOException {
        final Scene scene = this.onCreateScene();

        pOnCreateSceneCallback.onCreateSceneFinished(scene);
    }

    //    @Override
    public void onPopulateScene(Scene pScene, IGameInterface.OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }


    public synchronized void onResume() {
        super.onResume();

        acquireWakeLock();
    }


    @Override
    public synchronized void onWindowFocusChanged(final boolean pHasWindowFocus) {
        super.onWindowFocusChanged(pHasWindowFocus);

        if (pHasWindowFocus && this.mGamePaused && this.mGameCreated && !this.isFinishing()) {
            this.onResumeGame();
        }
    }


    public void onPause() {
        super.onPause();

        mEngine.disableAccelerationSensor(mActivity);
        releaseWakeLock();

        if (!this.mGamePaused) {
            this.onPauseGame();
        }
    }


    public void onDestroy() {
        mEngine.onDestroy();

        try {
            onDestroyResources();
        } catch (final Throwable ignored) {
        }

        onGameDestroyed();

        mEngine = null;
    }

    private void acquireWakeLock() {
        acquireWakeLock(mEngine.getEngineOptions().getWakeLockOptions());
    }

    private void acquireWakeLock(final WakeLockOptions pWakeLockOptions) {
        if (pWakeLockOptions == WakeLockOptions.SCREEN_ON) {
            ActivityUtils.keepScreenOn(mActivity);
        } else {
            final PowerManager pm = (PowerManager) mActivity.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(pWakeLockOptions.getFlag() | PowerManager.ON_AFTER_RELEASE, Constants.DEBUGTAG);
            try {
                mWakeLock.acquire();
            } catch (final SecurityException ignored) {
            }
        }
    }

    private void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
        }
    }

    public synchronized void onPauseGame() {
        mGamePaused = true;
        mEngine.stop();
    }

    public void onDestroyResources() throws IOException {
        if (mEngine.getEngineOptions().getAudioOptions().needsMusic()) {
            mEngine.getMusicManager().releaseAll();
        }

        if (mEngine.getEngineOptions().getAudioOptions().needsSound()) {
            mEngine.getSoundManager().releaseAll();
        }
    }

    public synchronized void onGameDestroyed() {
        this.mGameCreated = false;
    }

}
