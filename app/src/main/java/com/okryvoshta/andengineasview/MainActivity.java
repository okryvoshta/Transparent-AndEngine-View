package com.okryvoshta.andengineasview;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    private AndEngineView mAndEngineView;

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);

        setContentView(R.layout.main);

        mAndEngineView = (AndEngineView) findViewById(R.id.surface);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAndEngineView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAndEngineView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAndEngineView.onDestroy();
    }
}
